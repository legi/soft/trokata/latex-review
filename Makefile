####################################################################
###                                                              ###
###             Makefile pour  LaTeX                             ###
###                                                              ###
####################################################################
#
# Copyright 2000-2011 Gabriel Moreau / Vincent Morra

# Il y a encore pas mal de bug dans ce makefile
# g�n�ration du these.ps -> des merdouilles au niveau
# des figures. Solution: aller sous tmp/ et dvips � la main

#�General configuration
# Put your change only in config.mk file
################################################################

QUIX := A.linux

DOCUMENT := document

ARBO_TEX := ./
ARBO_FIG := $(ARBO_TEX)
ARBO_BIB := $(ARBO_TEX)

################################################################

DIR_PROJET := $(shell pwd | perl -p -e 's!/src/?.*$!!!' )
DIR_TMP    := $(DIR_PROJET)/tmp
DIR_DOC    := $(DIR_PROJET)/doc
DIR_PS     := $(DIR_DOC)/ps
DIR_PDF    := $(DIR_DOC)/pdf
DIR_HTML   := $(DIR_DOC)/html

################################################################

LATEX         := latex
BIBTEX        := bibtex
L2H           := latex2html
PDFTEX        := pdflatex
DVIPS         := dvips 
RM            := rm -f
BIN_PROTECT   := chmod u-w .
BIN_UNPROTECT := chmod u+w .

DVI_EDITOR := export XEDITOR="vim --servername DOC --remote +%l %f"
DVI_EDITOR := export XEDITOR="vim +%l %f"
DVI_EDITOR := export XEDITOR="nedit-nc -line %l %f"

DVI_VIEWER := xdvi
PDF_VIEWER := xpdf
PS_VIEWER  := gv


# User Config
################################################################

sinclude config.mk

# NE PAS TOUCHER A PARTIR D'ICI
################################################################

SRC_FIG     := $(notdir $(foreach dir,$(ARBO_FIG),$(wildcard $(dir)/*.fig)))
SRC_EPS     := $(notdir $(foreach dir,$(ARBO_FIG),$(wildcard $(dir)/*.eps)))
SRC_PDF     := $(notdir $(foreach dir,$(ARBO_FIG),$(wildcard $(dir)/*.pdf)))
SRC_JPG     := $(notdir $(foreach dir,$(ARBO_FIG),$(wildcard $(dir)/*.jpg)))
SRC_PNG     := $(notdir $(foreach dir,$(ARBO_FIG),$(wildcard $(dir)/*.png)))
SRC_GIF     := $(notdir $(foreach dir,$(ARBO_FIG),$(wildcard $(dir)/*.gif)))
SRC_TEX     := $(notdir $(foreach dir,$(ARBO_TEX),$(wildcard $(dir)/*.tex)))
SRC_BIB     := $(notdir $(foreach dir,$(ARBO_BIB),$(wildcard $(dir)/*.bib)))

OBJ_FIG2EPS      := $(patsubst %.fig,$(DIR_TMP)/%.eps,      $(SRC_FIG))
OBJ_FIG2PSTEX_T  := $(patsubst %.fig,$(DIR_TMP)/%.pstex_t,  $(SRC_FIG))
OBJ_FIG2PDF      := $(patsubst %.fig,$(DIR_TMP)/%.pdf,      $(SRC_FIG))
OBJ_FIG2PDFTEX_T := $(patsubst %.fig,$(DIR_TMP)/%.pdftex_t, $(SRC_FIG))
OBJ_EPS2PDF      := $(patsubst %.eps,$(DIR_TMP)/%.pdf,      $(SRC_EPS))
OBJ_JPG2EPS      := $(patsubst %.jpg,$(DIR_TMP)/%.eps,      $(SRC_JPG))
OBJ_PNG2EPS      := $(patsubst %.png,$(DIR_TMP)/%.eps,      $(SRC_PNG))
OBJ_GIF2EPS      := $(patsubst %.gif,$(DIR_TMP)/%.eps,      $(SRC_GIF))

OBJ_BASE    := $(DIR_TMP)/$(DOCUMENT)

TEXINPUTS   := $(subst //,/,$(subst / /,:/,$(foreach dir,$(ARBO_TEX) $(ARBO_FIG),$(strip $(DIR_PROJET)/src/$(dir)/)))):$(DIR_TMP):
BIBINPUTS   := $(subst //,/,$(subst / /,:/,$(foreach dir,$(ARBO_BIB),$(strip $(DIR_PROJET)/src/$(dir)/))))

PDFTEXPRE   := export TEXMFOUTPUT="$(DIR_TMP)";
PDFTEXPRE   += export TEXINPUTS="$(TEXINPUTS)";
PDFTEXPRE   += export LANG="C";

BIBTEXPRE   := cd $(DIR_TMP);
BIBTEXPRE   += BIBINPUTS="$(BIBINPUTS)"
BIBTEXPRE   += BSTINPUTS="$(BIBINPUTS)"

RERUNTEX := "(There were undefined references|Rerun to get (cross-references|the bars) right)"
RERUNBIB := "No file.*\.bbl|Citation.*undefined" 

COPYTOC := if test -r $(OBJ_BASE).toc; then cp $(OBJ_BASE).toc $(OBJ_BASE).toc.bak; fi 

vpath %.fig       $(ARBO_FIG)
vpath %.pstex     $(DIR_TMP)
vpath %.pstex_t   $(DIR_TMP)
vpath %.pdftex    $(DIR_TMP)
vpath %.pdftex_t  $(DIR_TMP)
vpath %.pdf       $(ARBO_FIG) $(DIR_TMP)
vpath %.eps       $(ARBO_FIG) $(DIR_TMP)
vpath %.jpg       $(ARBO_FIG)
vpath %.png       $(ARBO_FIG)
vpath %.gif       $(ARBO_FIG)
vpath %.bib       $(ARBO_BIB)

.PHONY: all folder
.PHONY: doc docps docpdf
.PHONY: html
.PHONY: xpdf xdvi gv
.PHONY: vims vimc
.PHONY: echo clean miniclean help
.PHONY: figure bibtex
.PHONY: pdflatex pdf  
.PHONY: latex    dvi
.PHONY: protect unprotect
.PHONY: warning review
.SUFFIXES:

#--------------------------------------------------------------#

all: folder doc

folder: $(DIR_HTML) $(DIR_PS) $(DIR_PDF) $(DIR_TMP)
	@echo '############################################################'
	@echo '### Fin de la gestion des dossiers'
	@echo '############################################################'

#--------------------------------------------------------------#

xpdf: $(DIR_TMP)/$(DOCUMENT).pdf
	$(PDF_VIEWER) $(DIR_TMP)/$(DOCUMENT).pdf &
	@echo '############################################################'
	@echo '####  Visualisation de $(DOCUMENT)'
	@echo '############################################################'

xdvi: $(DIR_TMP)/$(DOCUMENT).dvi
	$(DVI_EDITOR); \
	$(DVI_VIEWER) -bg grey95 $(DIR_TMP)/$(DOCUMENT) &
	@echo '############################################################'
	@echo '####  Visualisation de $(DOCUMENT)'
	@echo '############################################################'
#	xdvi -hl white $(DIR_TMP)/$(DOCUMENT) &

gv:$(DIR_PS)/$(DOCUMENT).ps 
	$(PS_VIEWER) $(DIR_PS)/$(DOCUMENT).ps &
	@echo '############################################################'
	@echo '####  Visualisation de $(DOCUMENT)'
	@echo '############################################################'

#--------------------------------------------------------------#

vims:
	vim --servername DOC $(DOCUMENT).tex

vimc:
	@echo alias vi=\'vim --servername DOC --remote\'

#--------------------------------------------------------------#

doc: docpdf docps
	@echo '############################################################'
	@echo '### Fin de la compilation '
	@echo '############################################################'

docpdf: $(DIR_PDF)/$(DOCUMENT).pdf
	@echo '############################################################'
	@echo '### Fin de la doc pdf'
	@echo '############################################################'

docps: $(DIR_PS)/$(DOCUMENT).ps 
	@echo '############################################################'
	@echo '### Fin de la doc postscript'
	@echo '############################################################'

html: $(DIR_HTML)
	$(L2H) -split 3 -local_icons -no_math -white \
        -tmp /tmp -dir $(DIR_HTML) -prefix $(DOCUMENT) -antialias $(DOCUMENT).tex

#--------------------------------------------------------------#

warning:
	@echo '############################################################'
	@echo '### Liste des warnings dans le documents'
	@echo '############################################################'
#	@touch -r $(DOCUMENT).tex $(DIR_TMP)/warning.log
#	@touch -r $(DIR_TMP)/warning.log
#	@touch $(DOCUMENT).tex
	@make pdflatex | grep Warning | grep -v Marginpar
#	@touch  -r $(DIR_TMP)/warning.log $(DOCUMENT).tex
#	@touch -r $(DIR_TMP)/warning.log 

review:
	@echo '############################################################'
	@echo "### Liste des `grep -HnE '\\rev(add|del|mark|par|forget)' -r . | grep -E '^*/.*\.tex\:' | grep -v macro_review.tex | wc -l` reviews dans le documents"
	@echo '############################################################'
	@grep -HnE '\\rev(add|del|mark|par|forget)' -r . | grep -E '^*/.*\.tex\:' | grep -v macro_review.tex

#--------------------------------------------------------------#

echo: 
	@echo '############################################################'
	@echo '### Liste des variables '
	@echo '############################################################'
	@echo SRC_FIG         $(SRC_FIG)
	@echo SRC_TEX         $(SRC_TEX)
	@echo OBJ_PSTEX       $(OBJ_PSTEX)
	@echo OBJ_FIG2PSTEX_T $(OBJ_FIG2PSTEX_T)
	@echo OBJ_EPS2PDF     $(OBJ_EPS2PDF)
	@echo OBJ_FIG2PDF     $(OBJ_FIG2PDF)

clean: 
	@echo '############################################################'
	@echo '####  Nettoyage'
	@echo '############################################################'
	$(RM) $(DOCUMENT).aux \
        $(DOCUMENT).log \
        $(DOCUMENT).bbl \
        $(DOCUMENT).blg \
        $(DOCUMENT).brf \
        $(DOCUMENT).cb  \
        $(DOCUMENT).ind \
        $(DOCUMENT).idx \
        $(DOCUMENT).ilg \
        $(DOCUMENT).dvi \
        $(DOCUMENT).inx \
        $(DOCUMENT).toc \
        $(DOCUMENT).out \
        $(DOCUMENT).lof \
        $(DOCUMENT).lot \
        $(DOCUMENT).pdf \
        $(OBJ_PSTEX) \
        $(OBJ_FIG2PSTEX_T) \
        $(OBJ_FIG2PDF) \
        $(DIR_PDF)/$(DOCUMENT).pdf \
        $(DIR_TMP)/*

miniclean: 
	@echo '############################################################'
	@echo '####  Nettoyage tres partiel'
	@echo '############################################################'
	$(RM)   $(DIR_TMP)/$(DOCUMENT).aux \
	        $(DIR_TMP)/$(DOCUMENT).toc \
	        $(DIR_TMP)/$(DOCUMENT).lof

#--------------------------------------------------------------#

figure: $(OBJ_FIG2PDF) $(OBJ_FIG2PDFTEX_T) $(OBJ_FIG2EPS) $(OBJ_FIG2PSTEX_T)
	@echo '############################################################'
	@echo '####  Fin des figures'
	@echo '############################################################'

#--------------------------------------------------------------#

protect:
	$(BIN_PROTECT)
	@echo '############################################################'
	@echo '#### Mise en place de la protection du dossier source'
	@echo '############################################################'

unprotect:
	$(BIN_UNPROTECT)
	@echo '############################################################'
	@echo '#### Suppression de la protection du dossier source'
	@echo '############################################################'

#--------------------------------------------------------------#

pdflatex: folder figure $(OBJ_EPS2PDF)
	@echo '############################################################'
	@echo '####  Debut du formateur pdflatex'
	@echo '############################################################'
	$(BIN_PROTECT)
	$(PDFTEXPRE) $(PDFTEX) $(DOCUMENT).tex; true
	$(BIN_UNPROTECT)
	@echo '############################################################'
	@echo '####  Fin du formateur pdflatex'
	@echo '############################################################'

latex: folder figure $(OBJ_JPG2EPS) $(OBJ_PNG2EPS) $(OBJ_GIF2EPS)
	@echo '############################################################'
	@echo '####  Debut du formateur latex'
	@echo '############################################################'
	$(BIN_PROTECT)
	$(PDFTEXPRE) $(LATEX) -src $(DOCUMENT).tex; true
	$(BIN_UNPROTECT)
	@echo '############################################################'
	@echo '####  Fin du formateur latex'
	@echo '############################################################'

bibtex:
	@echo '############################################################'
	@echo '####  Debut de la bibliographie'
	@echo '############################################################'
	$(BIBTEXPRE) $(BIBTEX) $(OBJ_BASE)
	@echo '############################################################'
	@echo '####  Fin de la bibliographie'
	@echo '############################################################'

#--------------------------------------------------------------#

help:
	@echo '############################################################'
	@echo '# all       -> compilation complete'
	@echo '# folder    -> cr�ation des dossiers du projet'
	@echo '# clean     -> nettoyage partiel'
	@echo '# pdf       -> compilation simple avec dependance -> pdf'
	@echo '# dvi       -> compilation simple avec dependance -> dvi'
	@echo '# xpdf      -> visualisation du resultat pdf'
	@echo '# xdvi      -> visualisation du resultat dvi'
	@echo '# gv        -> visualisation du resultat ps'
	@echo '# warning   -> liste de tous les warnings'
	@echo '# review    -> liste de toutes les remarques'
	@echo '# pdflatex  -> compilation du source tex -> pdf'
	@echo '# latex     -> compilation du source tex -> dvi'
	@echo '# bibtex    -> compilation du source bibtex'
	@echo '# protect   -> protection du dossier source'
	@echo '# unprotect -> suppression de la protection'
	@echo '############################################################'

#--------------------------------------------------------------#

pdf: $(DIR_TMP)/pdf.ok

dvi: $(DIR_TMP)/dvi.ok

$(DIR_TMP)/pdf.ok: $(DOCUMENT).tex $(SRC_TEX) $(OBJ_FIG2PDF) $(OBJ_FIG2PDFTEX_T)
	make pdflatex
	touch $<

$(DIR_TMP)/dvi.ok: $(DOCUMENT).tex $(SRC_TEX) $(OBJ_FIG2EPS) $(OBJ_FIG2PSTEX_T)
	make latex
	touch $<

################################################################

$(DIR_TMP)/$(DOCUMENT).pdf: $(DOCUMENT).tex $(SRC_BIB) $(SRC_TEX) $(OBJ_FIG2PDF) $(OBJ_FIG2PSTEX_T)
	$(COPYTOC)
	make pdflatex
	egrep -c $(RERUNBIB) $(OBJ_BASE).log && (make bibtex;$(COPYTOC); make pdflatex); true
	egrep $(RERUNTEX) $(OBJ_BASE).log    && ($(COPYTOC); make pdflatex); true
	egrep $(RERUNTEX) $(OBJ_BASE).log    && ($(COPYTOC); make pdflatex); true
	if cmp -s $(OBJ_BASE).toc $(OBJ_BASE).toc.bak; then echo 'end'; else make pdflatex; fi
	$(RM) $(OBJ_BASE).toc.bak
#	egrep -i "(Reference|Citation).*undefined" $(OBJ_BASE).log; true
# Display relevant warnings

################################################################

################################################################

$(DIR_TMP)/$(DOCUMENT).dvi: $(DOCUMENT).tex $(SRC_BIB) $(SRC_TEX) $(OBJ_FIG2EPS) $(OBJ_FIG2PSTEX_T)
	$(COPYTOC)
	make latex
	egrep -c $(RERUNBIB) $(OBJ_BASE).log && (make bibtex;$(COPYTOC); make latex); true
	egrep $(RERUNTEX) $(OBJ_BASE).log    && ($(COPYTOC); make latex); true
	egrep $(RERUNTEX) $(OBJ_BASE).log    && ($(COPYTOC); make latex); true
	if cmp -s $(OBJ_BASE).toc $(OBJ_BASE).toc.bak; then echo 'end'; else make latex; fi
	$(RM) $(OBJ_BASE).toc.bak
#	egrep -i "(Reference|Citation).*undefined" $(OBJ_BASE).log; true
# Display relevant warnings

################################################################

#$(DIR_PS)/%.ps: $(DIR_PDF)/%.pdf
#	pdftops $< $@


$(DIR_PS)/%.ps: $(DIR_TMP)/%.dvi 
	$(PDFTEXPRE) 
	dvips $< -o $@

$(DIR_PDF)/%.pdf: $(DIR_TMP)/%.pdf
	echo ---
	rm -f $@
	echo ---
	cp $< $@
#	ln -f $< $@
#       je sais pas pourquoi mais le lien ne marche pas bien avec le make

################################################################
# figure

#$(DIR_TMP)/%.pdf: $(DIR_TMP)/%.pstex $(DIR_TMP)/%.pstex_t
#	ps2pdf  $<  $@
#	cd $(DIR_TMP); \
#	sed 's/\.pstex/.pdf/' $*.pstex_t > $*.tmp; \
#	mv $*.tmp $*.pstex_t; \
#	sed 's/\\epsfig{file=/\\includegraphics{/' $*.pstex_t > $*.tmp; \
#	mv $*.tmp $*.pstex_t  

$(DIR_TMP)/%.pstex: %.fig
	fig2dev -L pstex $< $@

$(DIR_TMP)/%.eps: %.fig
	fig2dev -L pstex $< $@

$(DIR_TMP)/%.pstex_t: %.fig $(DIR_TMP)/%.pstex
	fig2dev -L pstex_t -p $* $< $@
#	fig2dev -L pstex_t -E 1 -p $* $< $@

#$(DIR_TMP)/%.pstex_t: $(DIR_TMP)/%.pstex %.fig 
#	fig2dev -L pstex_t -p $^ $@

ifeq "$(QUIX)" "A.linux"
$(DIR_TMP)/%.pdftex: %.fig
	fig2dev -L pdftex $< $@
endif
ifeq "$(QUIX)" "A.darwin"
$(DIR_TMP)/%.pdftex: %.fig
	fig2dev -L pstex $< $(patsubst %.pdftex,%.pstex,$@)
	ps2pdf $(patsubst %.pdftex,%.pstex,$@)  $@
endif

ifeq "$(QUIX)" "A.linux"
$(DIR_TMP)/%.pdf: %.fig
	fig2dev -L pdftex $< $@
endif
ifeq "$(QUIX)" "A.darwin"
$(DIR_TMP)/%.pdf: %.fig
	fig2dev -L pstex $< $(patsubst %.pdf,%.eps,$@)
	ps2pdf $(patsubst %.pdf,%.eps,$@)  $@
endif

$(DIR_TMP)/%.pdftex_t: %.fig $(DIR_TMP)/%.pdftex
	fig2dev -L pdftex_t -p $* $< $@
#	fig2dev -L pdftex_t -E 1 -p $* $< $@


$(DIR_TMP)/%.pdf: %.eps
	epstopdf $< --outfile $@

$(DIR_TMP)/%.eps: %.jpg
	convert $< $@

$(DIR_TMP)/%.eps: %.png
	convert $< $@

$(DIR_TMP)/%.eps: %.gif
	convert $< $@

################################################################
# dossier

$(DIR_DOC):
	mkdir -p $@
	chmod ugo+rx $@

$(DIR_TMP):
	mkdir -p $@
	chmod ugo+rwx $@

$(DIR_PS): $(DIR_DOC)
	mkdir -p $@
	chmod ugo+rx $@

$(DIR_PDF): $(DIR_DOC)
	mkdir -p $@
	chmod ugo+rx $@

$(DIR_HTML): $(DIR_DOC)
	mkdir -p $@
	chmod ugo+rx $@
